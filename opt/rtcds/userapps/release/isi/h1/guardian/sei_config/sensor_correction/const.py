from isiguardianlib import const as top_const

########################################
# Sensor Correction

SC_FM_confs = {
    'CS' : {
        'MANAGER' : {
            'SC_OFF' : { # off
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X' : 0,
                'Y' : 0,
                'Z' : 0},
            'WINDY' : { #nominal windy
                'X' : 1,
                'Y' : 1,
                'Z' : 2},
            'SWARM' : { #v2 eq sc
                'X' : 3,
                'Y' : 3,
                'Z' : 3},
            'EQ' : { #v2 eq sc
                'X' : 6,
                'Y' : 6,
                'Z' : 6},
        },
    },
    'ETMX' : {
        'BSC_ST1' : {
            'SC_OFF' : {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X' : 0,
                'Y' : 0,
                'Z' : None},
            'USEISM' : { # useism
                'X' : 9,
                'Y' : 9,
                'Z' : None},
            'WINDY' : { # Windy
                'X' : 1,
                'Y' : 9,
                'Z' : None},
            'EQ' : { # EQ
                'X' : 6,
                'Y' : 9,
                'Z' : None},
        },
        'HPI' : {
            'SC_OFF' : {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                 'X' : 0,
                'Y' : 0,
                'Z' : 0},
            'SC_ON' : {
                'X' : 0,
                'Y' : 0,
                'Z' : 1},
            'SC_EQ' : {
                'X' : 0,
                'Y' : 0,
                'Z' : 6},
           'CENTRAL' : { # CS
                'X' : 0,
                'Y' : 0,
                'Z' : 1},
        },  
    },
    'ETMY' : {
        'BSC_ST1' : {
            'SC_OFF' : {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X' : 0,
                'Y' : 0,
                'Z' : None},
            'USEISM' : { # useism
                'X' : 9,
                'Y' : 9,
                'Z' : None},
            'WINDY' : { # Windy
                'X' : 9,
                'Y' : 1,
                'Z' : None},
            'EQ' : { # EQ
                'X' : 9,
                'Y' : 6,
                'Z' : None},
        },
        'HPI' : {
            'SC_OFF' : {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X' : 0,
                'Y' : 0,
                'Z' : 0},
            'SC_ON' : {
                'X' : 0,
                'Y' : 0,
                'Z' : 1},
            'SC_EQ' : {
                'X' : 0,
                'Y' : 0,
                'Z' : 6},
           'CENTRAL' : { # CS
                'X' : 0,
                'Y' : 0,
                'Z' : 1},
        },
    },
    'ITMX' : {
        'BSC_ST1' : {
            'SC_OFF' : {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X' : 0,
                'Y' : 0,
                'Z' : None},
            'USEISM' : { # useism
                'X' : 9,
                'Y' : 9,
                'Z' : None},
            'WINDY' : { # Windy
                'X' : 1,
                'Y' : 1,
                'Z' : None},
            'EQ' : { # EQ
                'X' : 6,
                'Y' : 6,
                'Z' : None},
           'SWARM' : { # EQ
                'X' : 3,
                'Y' : 3,
                'Z' : None},
           'CENTRAL' : { # CS
                'X' : 10,
                'Y' : 10,
                'Z' : None},
        },
        'HPI' : {
            'SC_OFF' : {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X' : 0,
                'Y' : 0,
                'Z' : 0},
            'SC_ON' : {
                'X' : 0,
                'Y' : 0,
                'Z' : 1},
            'SC_EQ' : {
                'X' : 0,
                'Y' : 0,
                'Z' : 6},
           'CENTRAL' : { # CS
                'X' : 0,
                'Y' : 0,
                'Z' : 10},
        },
    },
    'ITMY' : {
        'BSC_ST1' : {
            'SC_OFF' : {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X' : 0,
                'Y' : 0,
                'Z' : None},
            'USEISM' : { # useism
                'X' : 9,
                'Y' : 9,
                'Z' : None},
            'WINDY' : { # Windy
                'X' : 1,
                'Y' : 1,
                'Z' : None},
            'EQ' : { # EQ
                'X' : 6,
                'Y' : 6,
                'Z' : None},
           'SWARM' : { # EQ
                'X' : 3,
                'Y' : 3,
                'Z' : None},
           'CENTRAL' : { # CS
                'X' : 10,
                'Y' : 10,
                'Z' : None},
        },
        'HPI' : {
            'SC_OFF' : {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X' : 0,
                'Y' : 0,
                'Z' : 0},
            'SC_ON' : {
                'X' : 0,
                'Y' : 0,
                'Z' : 1},
            'SC_EQ' : {
                'X' : 0,
                'Y' : 0,
                'Z' : 6},
           'CENTRAL' : { # CS
                'X' : 0,
                'Y' : 0,
                'Z' : 10},
        },
    },
    'BS' : {
        'BSC_ST1' : {
            'SC_OFF' : {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X' : 0,
                'Y' : 0,
                'Z' : None},
            'USEISM' : { # useism
                'X' : 9,
                'Y' : 9,
                'Z' : None},
            'WINDY' : { # Windy
                'X' : 1,
                'Y' : 1,
                'Z' : None},
            'EQ' : { # EQ
                'X' : 6,
                'Y' : 6,
                'Z' : None},
           'SWARM' : { # EQ
                'X' : 3,
                'Y' : 3,
                'Z' : None},
           'CENTRAL' : { # CS
                'X' : 10,
                'Y' : 10,
                'Z' : None},
        },
        'HPI' : {
            'SC_OFF' : {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X' : 0,
                'Y' : 0,
                'Z' : 0},
            'SC_ON' : {
                'X' : 0,
                'Y' : 0,
                'Z' : 1},
            'SC_EQ' : {
                'X' : 0,
                'Y' : 0,
                'Z' : 6},
           'CENTRAL' : { # CS
                'X' : 0,
                'Y' : 0,
                'Z' : 10},
        },
    },
    'HAM1' : {
        'HPI' : {
            'SC_OFF' : {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X' : 0,
                'Y' : 0,
                'Z' : 0},
            'SC_ON' : {
                'X' : 1,
                'Y' : 1,
                'Z' : 1},
            'SC_EQ' : {
                'X' : 6,
                'Y' : 6,
                'Z' : 6},
           'CENTRAL' : { # CS
                'X' : 10,
                'Y' : 10,
                'Z' : 10},
        },  
    },
    'HAM2' : {
        'HAM' : {
            'SC_OFF' : {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X' : 0,
                'Y' : 0,
                'Z' : 0},
            'WINDY' : { # Windy
                'X' : 2,
                'Y' : 2,
                'Z' : 1},
            'EQ' : { # EQ
                'X' : 5,
                'Y' : 5,
                'Z' : 5},
            'SWARM' : { # swarm test
                'X' : 3,
                'Y' : 3,
                'Z' : 1},
           'CENTRAL' : { # CS
                'X' : 10,
                'Y' : 10,
                'Z' : 10},
        },
    },
    'HAM3' : {
        'HAM' : {
            'SC_OFF' : {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X' : 0,
                'Y' : 0,
                'Z' : 0},
            'WINDY' : { # Windy
                'X' : 2,
                'Y' : 2,
                'Z' : 1},
            'EQ' : { # EQ
                'X' : 5,
                'Y' : 5,
                'Z' : 5},
            'SWARM' : { # swarm test
                'X' : 3,
                'Y' : 3,
                'Z' : 1},
           'CENTRAL' : { # CS
                'X' : 10,
                'Y' : 10,
                'Z' : 10},
        },
    },
    'HAM4' : {
        'HAM' : {
            'SC_OFF' : {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X' : 0,
                'Y' : 0,
                'Z' : 0},
            'WINDY' : { # Windy
                'X' : 2,
                'Y' : 2,
                'Z' : 1},
            'EQ' : { # EQ
                'X' : 5,
                'Y' : 5,
                'Z' : 5},
            'SWARM' : { # swarm test
                'X' : 3,
                'Y' : 3,
                'Z' : 1},
           'CENTRAL' : { # CS
                'X' : 10,
                'Y' : 10,
                'Z' : 10},
        },
    },
    'HAM5' : {
        'HAM' : {
            'SC_OFF' : {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X' : 0,
                'Y' : 0,
                'Z' : 0},
            'WINDY' : { # Windy
                'X' : 2,
                'Y' : 2,
                'Z' : 1},
            'EQ' : { # EQ
                'X' : 5,
                'Y' : 5,
                'Z' : 5},
            'SWARM' : { # swarm test
                'X' : 3,
                'Y' : 3,
                'Z' : 1},
           'CENTRAL' : { # CS
                'X' : 10,
                'Y' : 10,
                'Z' : 10},
        },
    },
    'HAM6' : {
        'HAM' : {
            'SC_OFF' : {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X' : 0,
                'Y' : 0,
                'Z' : 0},
            'WINDY' : { # Windy
                'X' : 2,
                'Y' : 2,
                'Z' : 1},
            'EQ' : { # EQ
                'X' : 5,
                'Y' : 5,
                'Z' : 5},
            'SWARM' : { # swarm test
                'X' : 3,
                'Y' : 3,
                'Z' : 1},
           'CENTRAL' : { # CS
                'X' : 10,
                'Y' : 10,
                'Z' : 10},
        },
    },
}
    

########################################
SC_FM_confs = SC_FM_confs[top_const.CHAMBER][top_const.CHAMBER_TYPE]
